<?php
use Phalcon\Mvc\Controller;
use Phalcon\Http\Response as Response;
use Models\Customer;
use Models\CustomerPhalcon;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Configuration;

class CustomerController extends Controller
{
    public function getConsumersDoctrineOrmAction()
    {
        $em = $this->di['entityManager'];

        $customers = $em->getRepository('Models\Customer')
            ->createQueryBuilder('c')
            ->select('c')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $this->returnResponse(json_encode($customers));
    }

    public function getConsumerDoctrineOrmAction($id)
    {
        $em = $this->di['entityManager'];

        /* @var $customer Customer */
        $customer = $em->getRepository('Models\Customer')->findOneById($id);

        $this->returnResponse(json_encode($customer->toArray()));
    }

    public function getConsumersPhalconOrmAction()
    {
        $customers = CustomerPhalcon::find()->toArray();

        $this->returnResponse(json_encode($customers));
    }

    public function getConsumersDoctrineDbalAction()
    {
        $conf = new Configuration;

        $params = array(
            'dbname' => 'ultimaker_phalcon',
            'user' => 'ultimakertest',
            'password' => 'test1234',
            'host' => 'localhost',
            'port' => 3306,
            'charset' => 'utf8',
            'driver' => 'pdo_mysql'
        );

        $conn = DriverManager::getConnection($params, $conf);
        $customers = $conn->query("SELECT * FROM customer");

        $this->returnResponse(json_encode($customers->fetchAll()));
    }

    public function getConsumerArrayAction()
    {
        $customers[]['id'] = '1';
        $customers[]['name'] = 'TestBv 1';
        $customers[]['id'] = '2';
        $customers[]['name'] = 'TestBv 2';

        $this->returnResponse(json_encode($customers));
    }

    private function returnResponse($data)
    {
        $response = new Response();
        $response->setContent($data);
        $response->send();
    }
}
