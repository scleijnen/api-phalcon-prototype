<?php

use Phalcon\Mvc\Url as UrlResolver;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return new \Phalcon\Config\Adapter\Ini(APP_PATH . "/config/config.ini");
});

/**
* Sets the view component required by phalcon micro framework but will not be used
*/
$di->setShared('view', function () {});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);
    return $url;
});
