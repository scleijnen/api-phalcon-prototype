<?php

$isDevMode = true;
$doctrineConfig = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
    array(__DIR__ . '/' . $config->application->modelsDir), $isDevMode
);

$conn = array(
    'driver'   => 'pdo_mysql',
    'path'     => __DIR__ . '/../' . $config->database->dbname,
    'user'     => $config->database->username,
    'password' => $config->database->password,
    'dbname'   => $config->database->dbname,
);

$entityManager = Doctrine\ORM\EntityManager::create($conn, $doctrineConfig);

$di->set('entityManager', function () use ($entityManager) {
    return $entityManager;
});

