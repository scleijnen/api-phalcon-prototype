<?php
error_reporting(E_ALL);

$hostname = 'localhost';
$username = 'ultimakertest';
$password = 'test1234';

try {
    $dbh = new PDO("mysql:host=127.0.0.1;dbname=ultimaker_phalcon",$username,$password);

    $sth = $dbh->prepare("SELECT * FROM customer");
    $sth->execute();
    $customers = $sth->fetchAll();

    print_r($customers);
} catch(PDOException $e) {
    echo $e->getMessage();
}
