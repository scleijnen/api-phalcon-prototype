<?php
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Micro;

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {
    /**
     * The FactoryDefault Dependency Injector automatically registers the services that
     * provide a full stack framework. These default services can be overidden with custom ones.
     */
    $di = new FactoryDefault();

    /**
     * Include Services
     */
    include APP_PATH . '/config/services.php';

    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();

    /**
     * Include Autoloader
     */
    include APP_PATH . '/config/loader.php';

    require_once __DIR__ . "/../vendor/autoload.php";

    /**
     * Doctrine entity manager
     */
    include __DIR__ . "/../config/doctrine_bootstrap.php";

    //include __DIR__ . "/../config/phalcon_bootstrap.php";

    /**
     * Starting the application
     * Assign service locator to the application
     */
    $app = new Micro($di);

    /**
     * Include Application
     */
    $consumerController = new CustomerController();

    // Retrieves all customers with phalcon orm
    $app->get(
        "/api/consumers_phalcon_orm",
        [
            $consumerController,
            "getConsumersPhalconOrmAction"
        ]
    );

    // Retrieves all customers with doctrine dbal
    $app->get(
        "/api/consumers_doctrine_dbal",
        [
            $consumerController,
            "getConsumersDoctrineDbalAction"
        ]
    );

    // Retrieves all customers with doctrine orm
    $app->get(
        "/api/consumers_doctrine_orm",
        [
            $consumerController,
            "getConsumersDoctrineOrmAction"
        ]
    );

    // Retrieves customers with doctrine orm based on primary key
    $app->get(
        "/api/consumers_doctrine_orm/{id:[0-9]+}",
        [
            $consumerController,
            "getConsumerDoctrineOrmAction"
        ]
    );

    // Retrieves array data
    $app->get(
        "/api/consumers_array",
        [
            $consumerController,
            "getConsumerArrayAction"
        ]
    );

    /**
     * Handle the request
     */
    echo $app->handle();

} catch (\Exception $e) {
      echo $e->getMessage() . '<br>';
      echo '<pre>' . $e->getTraceAsString() . '</pre>';
}
